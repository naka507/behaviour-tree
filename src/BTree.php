<?php
namespace BehaviourTree;

use BehaviourTree\Action\BTIdle;
use BehaviourTree\Action\BTIsNight;
use BehaviourTree\Action\BTNightRound;
use BehaviourTree\Action\BTOutside;
use BehaviourTree\Action\BTRain;
use BehaviourTree\Action\BTRoot;
use BehaviourTree\Action\BTUmbrella;
use BehaviourTree\Action\BTVigil;

class BTree
{
    public $Root;
    public $Ticker;

    public function __construct()
    {
        $this->Root = new BTRoot();
        $Umbrella = new BTUmbrella();

        $Umbrella->addChild(new BTOutside());
        $Umbrella->addChild(new BTRain());

        $this->Root->addChild($Umbrella);

        $Vigil = new BTVigil();

        $NightRound = new BTNightRound();
        $NightRound->addChild(new BTIsNight());

        $Vigil->addChild($NightRound);
        $Vigil->addChild(new BTIdle());

        $this->Root->addChild($Vigil);
    }

    public function start()
    {
        if (!$this->Ticker) {
            $this->Ticker = true;
            $this->setInterval('tick', 1000, $this);
        }
    }

    public function stop()
    {
        if ($this->Ticker) {
            $this->Ticker = false;
        }
    }

    public function tick()
    {
        echo '****************' . PHP_EOL;
        $out = [];
        $input = [];
        $rand = rand(1, 10);
        if ($rand > 3) {
            $input['rain'] = true;
        }
        $rand = rand(1, 10);
        if ($rand > 5) {
            $input['outside'] = true;
        }
        $rand = rand(1, 10);
        if ($rand > 8) {
            $input['night'] = true;
        }

        $this->Root->tick($input, $out);
    }

    public function setInterval($func, $milliseconds, $obj)
    {
        $seconds = (int) $milliseconds / 1000;
        while ($this->Ticker) {
            call_user_func([$obj, $func]);
            sleep($seconds);
        }
    }
}
