<?php

namespace BehaviourTree\Node;

class BTParallerNode extends BTNode
{
    public function tick($input, $out)
    {
        for ($i = 0; $i < count($this->Children); ++$i) {
            $this->Children[$i]->tick($input, $out);
        }

        return false;
    }
}
