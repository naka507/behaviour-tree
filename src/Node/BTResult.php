<?php

namespace BehaviourTree\Node;

class BTResult
{
    public static $Idle = 0;
    public static $Executing = 0;
    public static $Completed = 0;
    public static $Failed = '-1';
}
