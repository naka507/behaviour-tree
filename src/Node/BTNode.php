<?php

namespace BehaviourTree\Node;

class BTNode
{
    public $Parent;
    public $Children;

    public function __construct()
    {
        $this->Children = [];
    }

    public function tick($input, $out)
    {
        return true;
    }

    public function addChild($obj)
    {
        $obj->Parent = $this;
        $this->Children[] = $obj;
    }

    public function removeChild($obj)
    {
        for ($i = 0; $i < count($this->Children); ++$i) {
            if ($this->Children[$i] == $obj) {
                $obj->Parent = null;
                unset($this->Children[$i]);

                return true;
            }
        }

        return false;
    }
}
