<?php

namespace BehaviourTree\Node;

class BTActionNode extends BTNode
{
    public $State = 0;

    public function __construct()
    {
        parent::__construct();
        $this->State = BTResult::$Idle;
    }

    public function enter()
    {
        $this->State = BTResult::$Executing;
        $this->translateIn();

        return true;
    }

    public function exit()
    {
        $this->State = BTResult::$Completed;
        $this->translateOut();

        return true;
    }

    public function translateIn()
    {
    }

    public function translateOut()
    {
    }
}
