<?php

namespace BehaviourTree\Node;

class BTSelectorNode extends BTNode
{
    public function tick($input, $out)
    {
        for ($i = 0; $i < count($this->Children); ++$i) {
            if ($this->Children[$i]->tick($input, $out)) {
                return true;
            }
        }

        return false;
    }
}
