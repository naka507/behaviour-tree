<?php

namespace BehaviourTree\Node;

class BTSequenceNode extends BTNode
{
    public function tick($input, $out)
    {
        for ($i = 0; $i < count($this->Children); ++$i) {
            if (!$this->Children[$i]->tick($input, $out)) {
                return false;
            }
        }

        return true;
    }
}
