<?php

namespace BehaviourTree\Action;

use BehaviourTree\Node\BTConditionNode;

class BTIsNight extends BTConditionNode
{
    public function tick($input, $out)
    {
        echo 'BTIsNight'.PHP_EOL;

        return isset($input['night']);
    }
}
