<?php

namespace BehaviourTree\Action;

use BehaviourTree\Node\BTConditionNode;

class BTOutside extends BTConditionNode
{
    public function tick($input, $out)
    {
        echo 'BTOutside'.PHP_EOL;
        
        return isset($input['outside']);
    }
}
