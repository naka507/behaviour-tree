<?php

namespace BehaviourTree\Action;

use BehaviourTree\Node\BTConditionNode;

class BTRain extends BTConditionNode
{
  public function tick($input, $out)
  {
      echo 'BTRain'.PHP_EOL;

      return isset($input['rain']);
  }
}
