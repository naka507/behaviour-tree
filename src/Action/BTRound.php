<?php

namespace BehaviourTree\Action;

use BehaviourTree\Node\BTActionNode;

class BTRound extends BTActionNode
{
  public function tick()
  {
      echo 'BTRound'.PHP_EOL;

      return true;
  }
}
