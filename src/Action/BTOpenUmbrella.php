<?php

namespace BehaviourTree\Action;

use BehaviourTree\Node\BTActionNode;

class BTOpenUmbrella extends BTActionNode
{
    public function tick($input, $out)
    {
        echo 'BTOpenUmbrella'.PHP_EOL;

        return true;
    }
}
