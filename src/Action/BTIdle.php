<?php
namespace BehaviourTree\Action;

use BehaviourTree\Node\BTActionNode;

class BTIdle extends BTActionNode
{
    public function tick($input, $out)
    {
        echo 'BTIdle' . PHP_EOL;

        return true;
    }
}
